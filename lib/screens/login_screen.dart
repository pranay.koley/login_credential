// ignore_for_file: prefer_const_constructors, use_build_context_synchronously, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:login_credential/screens/home_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LogInScreen extends StatefulWidget {
  const LogInScreen({super.key});

  @override
  State<LogInScreen> createState() => _LogInScreenState();
}

class _LogInScreenState extends State<LogInScreen> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController numberController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  bool isCheck = false;

  RegExp passValid = RegExp(r"(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*\W)");
  bool validatePassword(String pass) {
    String password = pass.trim();
    if (passValid.hasMatch(password)) {
      return true;
    } else {
      return false;
    }
  }

  @override
  void initState() {
    super.initState();
    getCheckBox();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      setState(() {});
      isCheck = await getCheckBox();
      debugPrint('in get checkbox ${isCheck.toString()}');
      if (isCheck) {
        getNumber();
        getPassword();
        WidgetsBinding.instance.addPostFrameCallback((_) async {
          numberController.text = await getNumber();
          passwordController.text = await getPassword();
        });
      }
    });
    debugPrint('in init ${isCheck.toString()}');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
          child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'WELCOME !',
              style: TextStyle(fontSize: 35),
            ),
            SizedBox(
              height: 100,
            ),

            //NUMBER FIELD
            Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 25, vertical: 15),
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white70,
                      border: Border.all(),
                      borderRadius: BorderRadius.circular(20)),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: TextFormField(
                      controller: numberController,
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                        FilteringTextInputFormatter.digitsOnly
                      ],
                      keyboardType: TextInputType.phone,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter some text';
                        } else if (value.length > 15 || value.length < 10) {
                          return 'Enter valid number';
                        }
                        return null;
                      },
                      style: TextStyle(
                          fontSize: 20,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                      obscureText: false,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintStyle: TextStyle(
                              color: Color.fromARGB(76, 0, 0, 0),
                              fontWeight: FontWeight.normal),
                          contentPadding: EdgeInsets.zero,
                          hintText: 'Enter your number'),
                    ),
                  ),
                )),

            //PASSWORD FIELD
            Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 25, vertical: 15),
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white70,
                      border: Border.all(),
                      borderRadius: BorderRadius.circular(20)),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: TextFormField(
                      controller: passwordController,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter password';
                        } else {
                          bool result = validatePassword(value);
                          if (result) {
                            // create account event
                            return null;
                          } else {
                            return "Should contain Capital, small letter, Number & Special character";
                          }
                        }
                      },
                      style: TextStyle(
                          fontSize: 20,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                      obscureText: true,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintStyle: TextStyle(
                              color: Color.fromARGB(76, 0, 0, 0),
                              fontWeight: FontWeight.normal),
                          contentPadding: EdgeInsets.zero,
                          hintText: 'Enter your password'),
                    ),
                  ),
                )),

            //CHECKBOX
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 5),
              child: Row(
                children: [
                  Text(
                    "Check to save credential.",
                  ),
                  Checkbox(
                    value: isCheck,
                    onChanged: (checkValue) {
                      setState(() {
                        isCheck = checkValue!;
                        debugPrint(isCheck.toString());
                      });
                    },
                  )
                ],
              ),
            ),

            //BUTTON FIELD
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 15),
              child: GestureDetector(
                onTap: () async {
                  debugPrint(isCheck.toString());
                  if (_formKey.currentState!.validate()) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                          duration: Duration(seconds: 1),
                          content: Text('Processing Data')),
                    );
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                              HomeScreen(userNumber: numberController.text),
                        ));
                    var sharedPreferences =
                        await SharedPreferences.getInstance();
                    if (isCheck) {
                      sharedPreferences.setBool('checkBox', isCheck);

                      sharedPreferences.setString(
                          "number", numberController.text.toString());
                      sharedPreferences.setString(
                          "password", passwordController.text.toString());
                      debugPrint('in condition ${isCheck.toString()}');
                    } else {
                      sharedPreferences.setBool('checkBox', isCheck);
                    }
                  }
                },
                child: Container(
                    height: 40,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(15)),
                    child: Center(
                      child: Text(
                        'LOGIN',
                        style: TextStyle(
                            color: Colors.amber.shade100, fontSize: 20),
                      ),
                    )),
              ),
            )
          ],
        ),
      )),
    );
  }

  static getNumber() async {
    var sharedPreferences = await SharedPreferences.getInstance();
    var getNumber = sharedPreferences.getString("number");
    // var getPassword = sharedPreferences.getString("password");
    return getNumber;
  }

  static getPassword() async {
    var sharedPreferences = await SharedPreferences.getInstance();
    var getPassword = sharedPreferences.getString("password");

    return getPassword;
  }

  static getCheckBox() async {
    var sharedPreferences = await SharedPreferences.getInstance();
    var getCheckBox = sharedPreferences.getBool('checkBox');
    debugPrint('in checkbox func ${getCheckBox.toString()}');
    return getCheckBox;
  }
}
