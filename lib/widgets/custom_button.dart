// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

Widget customButton({
  required String buttonName,
  required BuildContext context,
}) {
  return Container(
      height: 40,
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.red, borderRadius: BorderRadius.circular(15)),
      child: Center(
        child: Text(
          buttonName,
          style: TextStyle(color: Colors.amber.shade100, fontSize: 20),
        ),
      ));
}
