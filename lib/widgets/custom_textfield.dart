// ignore_for_file: public_member_api_docs, sort_constructors_first
// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  String hintText;
  bool obscureText;
  GlobalKey formKey;
  CustomTextField({
    Key? key,
    required this.hintText,
    required this.obscureText,
    required this.formKey,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white70,
          border: Border.all(),
          borderRadius: BorderRadius.circular(20)),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: TextFormField(
          key: formKey,
          style: TextStyle(
              fontSize: 20, color: Colors.black, fontWeight: FontWeight.bold),
          obscureText: obscureText,
          decoration: InputDecoration(
              border: InputBorder.none,
              hintStyle: TextStyle(
                  color: Color.fromARGB(76, 0, 0, 0),
                  fontWeight: FontWeight.normal),
              contentPadding: EdgeInsets.zero,
              hintText: hintText),
        ),
      ),
    );
  }
}
